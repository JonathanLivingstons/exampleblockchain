const SHA256 = require('crypto-js/sha256');
console.log('no its final');
console.loo('one more thing');
class Block {
  constructor(index, timestamp, data, previousHash = '') {
    this.index = index;
    this.timestamp = timestamp;
    this.data = data;
    this.previousHash = previousHash;
    this.hash = '';
  }
  calculateHash() {
    return SHA256(
      this.index +
        this.previousHash +
        this.timestamp +
        JSON.stringify(this.data)
    ).toString();
  }
}
class Blockchain {
  constructor() {
    this.chain = [this.createGenesisBlock()];
  }
  createGenesisBlock() {
    return new Block(0, '01/01/2019', 'Genesis data of blockchain!!!', '0');
  }
  getLastestBlock() {
    return this.chain[this.chain.length - 1];
  }
  addBlock(newBlock) {
    newBlock.previousHash = this.getLastestBlock().hash;
    newBlock.hash = newBlock.calculateHash();
    this.chain.push(newBlock);
  }
  isValid() {
    for (let i = 1; i < this.chain.length; i++) {
      const currentBlock = this.chain[i];
      const previousBlock = this.chain[i - 1];
      if (currentBlock.hash !== currentBlock.calculateHash()) {
        console.log('current');
        console.log(currentBlock.hash);
        console.log('calculate');
        console.log(currentBlock.calculateHash());
        console.log('-----');
        console.log('a');
        return false;
      }
      if (currentBlock.previousHash !== previousBlock.hash) {
        console.log('b');
        return false;
      }
    }
    return true;
  }
}
let bitKuy = new Blockchain();
bitKuy.addBlock(new Block(1, '01/02/2019', { coin: 10 }));
bitKuy.addBlock(new Block(2, '01/02/2019', { coin: 40 }));
bitKuy.addBlock(new Block(3, '01/03/2019', { coin: 4 }));
bitKuy.addBlock(new Block(4, '01/03/2019', { coin: 4 }));
//bitKuy.chain[3].data = {kuy:100}
console.log(bitKuy.isValid());
// console.log(JSON.stringify(bitKuy,null,5))
